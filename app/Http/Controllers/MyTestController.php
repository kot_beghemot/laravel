<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyTestController extends Controller
{
    public function index()
    {
        return CLASS . '=>' . FUNCTION;
    }
    public function all($id)
    {
        return view('test', compact('id'));
    }
}
